compilar:limpiar
	mkdir bin
	find src | grep .java | xargs javac -cp bin -d bin
jar:compilar
	jar cvfm ap-personas.jar Manifest.txt -C bin .
ejecutar: compilar
	java -cp bin aplicacion.Principal
limpiar:
	rm -rf bin
