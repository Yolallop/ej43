package dominio;

public class Localidad{

	/**
	 *Creacion de 2 variables 
	 */
	private String nombre;
        private int numeroDeHabitantes;
	
	
        public String getNombre(){
                return nombre;

        }
        public void setNombre(String nombre){
                this.nombre= nombre;
        }


        public int getNumeroDeHabitantes(){
                return numeroDeHabitantes;
        }

        public void setNumeroDeHabitantes(int numeroDeHabitantes){
                this.numeroDeHabitantes =numeroDeHabitantes;
        }


	/**
	 * Metodo para devolver la informacion sobre el nombre de localidades y su numero de habitantes
	 * @return mensaje
	 */
        public String toString(){
                String mensaje = "El nombre de la localidad es" + nombre;
                        mensaje += "tiene un numero de Habitantes" + numeroDeHabitantes;
                return mensaje;
        }
}

