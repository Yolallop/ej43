package dominio;

import java.util.ArrayList;

public class Municipio{
			
	private String nombre;
	private ArrayList<Localidad> localidades = new ArrayList<>();
	/**
	 *Metodo para calcular el numero de habitantes 
	 */
	public int calcularNumeroDeHabitantes() {
				
		int numeroDeHabitantes= 0;
		for (int i=0;i < localidades.size(); i++) { 
			numeroDeHabitantes += localidades.get(i).getNumeroDeHabitantes();
		}
		return numeroDeHabitantes;
	
	}

	/**
	 *Creacin de el get de la variable nombre
	 */
	public String getNombre(){
		return nombre;
	}
	/**
	 *creacion del set d ela variable nombre 
	 */

	public void setNombre(String nombre){
		this.nombre = nombre;

	}
	/**
	 *creacion del get de la ArrayList
	 */ 

	public ArrayList<Localidad> getLocalidades(){
		return localidades;
	}
	/**
	 *
	 * creacion del set de la ArrayList
	 */

	public void setLocalidades(ArrayList<Localidad> localidades){
		this.localidades = localidades;
	}

	/**
	 *Metodo de agregacion de una Localidad
	 */
	public void addLocalidad(Localidad localidad){
		localidades.add(localidad);
	}
	/**
	 *Metodo toString para devolver las localidades dentro de cada municipio
	 */
	public String toString(){

		String mensaje  = "Las localidades del Municipio tienen un numero de habitantes";
		for(int i = 0; i < localidades.size();i++){
			mensaje += localidades.get(i);
			
		}
		return mensaje;
	}
}
