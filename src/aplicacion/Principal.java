package aplicacion;
import dominio.*;
/**
 * @author Yolanda Llop
 * @version 26/10/21
 */
public class Principal{
	public static void main(String args[]){
	/*
	 *Creacion de una localidad 
	 */
	Localidad reus = new Localidad();
	reus.setNombre("Reus");
	reus.setNumeroDeHabitantes(9000);
	/*
	 *Creacion de un municipio
	 */
	Municipio municipio1 = new Municipio();
	municipio1.setNombre("Barcelona");
	municipio1.addLocalidad(reus);


	System.out.println(municipio1);
	}
}
